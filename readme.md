# **API REST DIAN 2 - UBL 2.1 - VALIDACIÓN PREVIA**

## Acerca de FACTURALATAM - ACTUALIZACION

Estamos enfocados en crear herramientas y soluciones para desarrolladores de software, empresas medianas y pequeñas, tenemos un sueño, queremos democratizar la facturación electrónica.


## 1.- Proceso de habilitación DIAN Validación Previa UBL 2.1

Obtener parámetros para software directo - [Aquí](https://youtu.be/0ThLJBzWyUA)


## 2.- Ejemplos para la API / Collection POSTMAN

 Primero descargue la herramienta POSTMAN desde internet, y luego importe el archivo collection aquí [postman collection](https://gitlab.com/torresoftware/co-apidian2021/-/blob/master/ApiDianV2.1%20FacturaLatam.postman_collection.json "Clic") 


## 3.- Videos para la instalación y despliegue en local (Windows)
* Factura electrónica Colombia DIAN validación previa - PHP (1/4) Introducción [Aquí](https://www.youtube.com/watch?v=FwBaQfI2ghw)
* Factura electrónica Colombia DIAN validación previa - PHP (2/4) Instalación [Aquí](https://youtu.be/-DMnOKSaWr8)
* Factura electrónica Colombia DIAN validación previa - PHP (3/4) Configuración API [Aquí](https://youtu.be/CAyZ9suZLII)
* Factura electrónica Colombia DIAN validación previa - PHP (4/4) Envío Factura [Aquí](https://youtu.be/28l74DvL8_o)
* Video que demuestra funcionalidad de firma, envio y consulta de estado de documentos XML [Aqui](https://drive.google.com/open?id=15-saHPT-NsWZEWrNMciteSU9bBGh_Msf)


## 4.- Manuales de instalación en VPS LINUX

* Instalacion bajo un solo entorno [Hetzner/Contabo/Linux:](https://docs.google.com/document/d/1hz3W9dn7HJJD9NSfo6zc4jjidQoPtSJZmoljUbqIy94/edit "Clic")
* Instalacion bajo entorno [Windows:](https://drive.google.com/file/d/1gaGLi8xcm8wHlqT-qzAYD00_AZJRmpcW/view?usp=sharing "Clic")
<!-- * Instalacion bajo entorno [Linux:](https://drive.google.com/file/d/1xxca_QkLtS1xlh5Rd9QFcDEJKmoIrfgI/view?usp=sharing "Clic") -->
* Instalacion bajo entorno [Google VPS:](https://drive.google.com/file/d/1KcVyhhyFhnr-fzIX9gCUX9OT1CqfAyMB/view?usp=sharing "Clic")


## 5.- Cómo pasar a producción 

* [Guía](https://www.youtube.com/watch?v=gBtd4XqwWtg)


## Actualizar desde el api 2020 al api 2021 
 
* Proceso de Actualizacion apidian2020 -> apidian2021 (https://drive.google.com/file/d/13kSbIqM7HDTYOMqhvCpQ0ZyzYxm7bYpB/view?usp=sharing "Clic")
