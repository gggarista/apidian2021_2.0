<table style="width: 100%">
    <tbody>
        <tr>
            <td style="width: 30%; text-align: center;">
                <p style="font-weight: bold; font-size: 11px;">NOTA CREDITO DE LA FACTURA ELECTRONICA DE VENTA No</p>
                <p style="color: red;
                    font-weight: bold;
                    font-size: 16px;
                    text-align: center;
                    padding: 5px;">{{$resolution->prefix}} - {{$request->number}}</p>
                <p style="font-size: 8px;">Fecha Validacion DIAN: {{$date}}<br>
                    Hora Validacion DIAN: {{$time}}</p>
            </td>
            <td style="width: 40%; text-align: center; padding: 0 10px;">
                <div id="empresa-header">
                    <strong>{{$user->name}}</strong><br>
                    @if(isset($request->establishment_name))
                        <strong>{{$request->establishment_name}}</strong><br>
                    @endif
                </div>
                <div id="empresa-header1">
                    @if(isset($request->ivaresponsable))
                        @if($request->ivaresponsable != $company->type_regime->name)
                            NIT: {{$company->identification_number}}-{{$company->dv}} - {{$company->type_regime->name}} - {{$request->ivaresponsable}} - {{$company->type_liability->name}}<br>
                        @else
                            NIT: {{$company->identification_number}}-{{$company->dv}} - {{$company->type_regime->name}} - {{$company->type_liability->name}}<br>
                        @endif
                    @else
                        NIT: {{$company->identification_number}} - {{$company->type_regime->name}} - {{$company->type_liability->name}}<br>
                    @endif
                    @if(isset($request->nombretipodocid))
                        Tipo Documento ID: {{$request->nombretipodocid}}<br>
                    @endif
                    @if(isset($request->tarifaica)  && $request->tarifaica != '100')
                        TARIFA ICA: {{$request->tarifaica}}%
                    @endif
                    @if(isset($request->tarifaica) && isset($request->actividadeconomica))
                        -
                    @endif
                    @if(isset($request->actividadeconomica))
                        ACTIVIDAD ECONOMICA: {{$request->actividadeconomica}}<br>
                    @else
                        <br>
                    @endif
                    REPRESENTACION GRAFICA DE NOTA CREDITO ELECTRONICA<br>
                    {{$company->address}} - {{$company->municipality->name}} - {{$company->country->name}} Telefono - {{$company->phone}}<br>
                    E-mail: {{$user->email}} <br>
                </div>
            </td>
            <td style="width: 30%; text-align: right;">
                <img src="{{$imgLogo}}" style="width: 136px; height: auto;" alt="logo">
            </td>
        </tr>
    </tbody>
</table>
