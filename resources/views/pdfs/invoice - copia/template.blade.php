<!DOCTYPE html>
<html lang="uk">
{{-- <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>FACTURA ELECTRONICA Nro: {{$resolution->prefix}} - {{$request->number}}</title>
</head> --}}

<body>
    <table class="table" style="width: 100%">
        <tbody  >
            <tr>
                <td  style="width: 70px; font-size: 10px;"  >
                    C.C. o NIT:<br>
                    Cliente:<br>
                    @if(isset($request->ccliente))
                        {{$request->ccliente}}
                    @else
                        <br>
                    @endif
                    Dirección:<br>
                    Vendedor:<br>
                    E-mail:
                </td>
                    <td  style="width: 55%; font-size: 10px" >
                    {{$customer->company->identification_number}}-{{$request->customer["dv"] ?? NULL}}
                    <br/>
                    {{$customer->name}}<br/>
                    {{$request->sucursal}}
                    <br/>
                    {{$customer->company->address}}  {{$customer->company->municipality->name}} - {{$customer->company->country->name}}<br>
                    {{ $request->cvendedor}}<br/>
                    {{$customer->email}}
                </td>
                <td rowspan="2" style="width: 40px;  font-size: 9px" >
                    Pedido: <br>
                    Teléfono:<br>
                    Régimen:<br>
                    Forma Pago:<br>
                    Plazo Pago:<br>
                    Vencimiento:
                </td>
                <td   rowspan="2" style="width: 150px; font-size: 9px ">
                    {{$request->cpedido }}<br>
                    {{$customer->company->phone}}<br>
                    {{$customer->company->type_regime->name}}<br>
                    {{$paymentForm->name}}-{{$paymentForm->nameMethod}}<br>
                    {{$paymentForm->duration_measure}} Dias<br>
                    {{$paymentForm->payment_due_date}}
                </td>
                <td rowspan="2" style="width: 100px; border: none; background: none;  " >
                    <img id="imgQR" src="{{$imageQr}}">
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table" style="width: 100%;">
        <thead  >
            <tr>
                <th class="text-center">#</th>
                <th class="text-center" >Código</th>
                <th class="text-center">Descripcion</th>
                <th class="text-center">Cantidad</th>
                <th class="text-center">UM</th>
                <th class="text-center">Val. Unit</th>
                <th class="text-center">IVA/IC</th>
                <th class="text-center">V.Desc.</th>
                <th class="text-center">Val. Item</th>
            </tr>
        </thead>
        <tbody>
            <?php $ItemNro = 0; ?>
            @foreach($request['invoice_lines'] as $item)
                <?php $ItemNro = $ItemNro + 1; ?>
                <tr>
                    @inject('um', 'App\UnitMeasure')
                    @if($item['description'] == 'Administración' or $item['description'] == 'Imprevisto' or $item['description'] == 'Utilidad')
                        <td>{{$ItemNro}}</td>
                        <td class="text-right">
                            {{$item['code']}}
                        </td>
                        <td>{{$item['description']}}</td>
                        <td class="text-right">{{number_format($item['price_amount'], 2)}}</td>
                        <td class="text-right">{{number_format($item['tax_totals'][0]['tax_amount'], 2)}}</td>
                        @if(isset($item['allowance_charges']))
                            <td class="text-right">{{number_format($item['allowance_charges'][0]['amount'], 2)}}</td>
                        @else
                            <td class="text-right">{{number_format("0", 2)}}</td>
                        @endif
                        <td class="text-right">{{number_format($item['invoiced_quantity'] * $item['price_amount'], 2)}}</td>
                    @else
                        <td>{{$ItemNro}}</td>
                        <td>{{$item['code']}}</td>
                        <td>{{$item['description']}}</td>
                        <td class="text-right">{{number_format($item['invoiced_quantity'], 2)}}</td>
                        <td class="text-right">{{$um->findOrFail($item['unit_measure_id'])['name']}}</td>
                        <td class="text-right">{{number_format($item['price_amount'], 2)}}</td>
                        @if(isset($item['tax_totals']))
                            @if(isset($item['tax_totals'][0]['tax_amount']))
                                <td class="text-right">{{number_format($item['tax_totals'][0]['tax_amount'], 2)}}</td>
                            @else
                                <td class="text-right">{{number_format(0, 2)}}</td>
                            @endif
                        @else
                            <td class="text-right">E</td>
                        @endif
                        @if(isset($item['allowance_charges']))
                            <td class="text-right">{{number_format($item['allowance_charges'][0]['amount'], 2)}}</td>
                        @else
                            <td class="text-right">{{number_format("0", 2)}}</td>
                        @endif
                        <td class="text-right">{{number_format($item['invoiced_quantity'] * $item['price_amount'], 2)}}</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
    <table class="table" id="tablaTotales">
        <tbody>
            <tr>
                <td style="width: 27%;">
                    <table class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center">Nro Lineas: {{$ItemNro}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="border: none">
                                    <span style="color: #a09e9e" >
                                        Recibi real y materialmente las mercancias descritas arriba a satisfacion. Se hace constar que la firma de persona distinta al comprador supone que esta autorizado para firmar , recibir y confesar la deuda y obligar al comprador.
                                    </span>
                                    <br><br>
                                firma:_______________     Identificacion:_______________
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </td>
                <td style="width: 50%;">
                    <table class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center">Impues.</th>
                                <th class="text-center">Base</th>
                                <th class="text-center">Valor</th>
                                <th class="text-center">Retenc.</th>
                                <th class="text-center">Base</th>
                                <th class="text-center">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($request->tax_totals))
                                <?php $TotalImpuestos = 0; ?>
                                @foreach($request->tax_totals as $item)
                                    <tr>
                                        <?php $TotalImpuestos = $TotalImpuestos + $item['tax_amount'] ?>
                                        @inject('tax', 'App\Tax')
                                        <td>{{$tax->findOrFail($item['tax_id'])['name']}}-<small>{{number_format($item['percent'], 2)}}%</small> </td>
                                        <td class="text-right" >{{number_format($item['taxable_amount'], 2)}}</td>
                                        <td class="text-right" >{{number_format($item['tax_amount'], 2)}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <?php $TotalImpuestos = 0; ?>
                            @endif
                            @if(isset($withHoldingTaxTotal))
                                <?php $TotalRetenciones = 0; ?>
                                @foreach($withHoldingTaxTotal as $item)
                                    <tr>
                                        <?php $TotalRetenciones = $TotalRetenciones + $item['tax_amount'] ?>
                                        @inject('tax', 'App\Tax')
                                        <td>{{$tax->findOrFail($item['tax_id'])['name']}}-<small>{{number_format($item['percent'], 2)}}%</small> </td>
                                        <td  class="text-right" >{{number_format($item['taxable_amount'], 2)}}</td>
                                        <td class="text-right" >{{number_format($item['tax_amount'], 2)}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            <tr>
                                @inject('Varios', 'App\Custom\NumberSpellOut')
                                @if(isset($request->tarifaica))
                                    <td colspan="6" class="totalf">SON: {{$Varios->convertir($request->legal_monetary_totals['payable_amount'] + $request->legal_monetary_totals['allowance_total_amount'])}} M/CTE*********.</td>
                                @else
                                    <td colspan="6" class="totalf">SON: {{$Varios->convertir($request->legal_monetary_totals['payable_amount'])}} M/CTE*********.</td>
                                @endif
                            </tr>
                            <tr>
                                @if(isset($notes))
                                    <td colspan="6" class="totalf">{{$notes}} </td>
                                @endif
                            </tr>
                    </tbody>
                    </table>
                </td>
                <td style="width: 17%;">
                    <table class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center">Totales</th>
                                <th class="text-center">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($request->legal_monetary_totals['line_extension_amount'] > 0)
                                <tr>
                                    <td>Base:</td>
                                    <td class="text-right" >{{number_format($request->legal_monetary_totals['line_extension_amount'], 2)}}</td>
                                </tr>
                            @endif

                            @if($TotalImpuestos > 0)
                                <tr>
                                    <td>Impuestos:</td>
                                    <td class="text-right" >{{number_format($TotalImpuestos, 2)}}</td>
                                </tr>
                             @endif
                             @if($TotalRetenciones > 0)
                                <tr>
                                    <td>Retenciones:</td>
                                    <td class="text-right" >{{number_format($TotalRetenciones, 2)}}</td>
                                </tr>
                             @endif

                             @if($request->legal_monetary_totals['allowance_total_amount'] > 0)
                                <tr>
                                    <td>Descuentos:</td>
                                    <td class="text-right" >{{number_format($request->legal_monetary_totals['allowance_total_amount'], 2)}}</td>
                                </tr>
                            @endif
                            <tr>
                                <td class="totalf">Total Factura:</td>
                                @if(isset($request->tarifaica))
                                    <td class="totalf" style="text-align: right">{{number_format($request->legal_monetary_totals['payable_amount'] + $request->legal_monetary_totals['allowance_total_amount'], 2)}}</td>
                                @else
                                    <td class="totalf" style="text-align: right">{{number_format($request->legal_monetary_totals['payable_amount'], 2)}}</td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="text-word" id="note" style="padding: 0px; margin: 0; font-size: 10px">
        @if(isset($imgM))
            <img  style="width: 100%; height: 10%; padding: 0px" src="{{$imgM}}" alt="mensaje nota">
        @endif
    </div>
</body>
</html>

