<table width="100%">
    <tr>
        <td style="width: 70%; padding: 0 1rem;" class="text-center vertical-align-top">
            <div  style="width: 70%; height: auto;" >
                <img  style="width: 70%; height: 12.5%;" src="{{$imgLogo}}" alt="logo">
            </div>
        <td style="width: 30%;" class="text-center vertical-align-top summarys">
            <div id="reference">
                <p>Pag {PAGENO} de {nbpg}</p>
                <p style="font-weight: 200;"><strong>Representación Gráfica</strong></p>
                <p style="font-weight: 700;"><strong>FACTURA ELECTRONICA DE VENTA No</strong></p>
                <p id="factnum" style="padding: 10px">
                    {{$resolution->prefix}} - {{$request->number}}</p>
                <p>Fecha Validacion DIAN: {{$date}}<br>
                   Hora Validacion DIAN: {{$time}}</p>
            </div>
        </td>
    </tr>
</table>
<div width="100%" style="font-size: 8px">
        Resolución Facturación Electrónica No. {{$resolution->resolution}}
        de {{$resolution->resolution_date}} Prefijo {{$resolution->prefix}}, Rango {{$resolution->from}} Al {{$resolution->to}} - Vigencia Desde: {{$resolution->date_from}} Hasta: {{$resolution->date_to}}
</div>
