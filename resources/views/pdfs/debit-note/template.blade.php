<<<<<<< HEAD
<!DOCTYPE html>
<html lang="uk"> 
{{-- <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>FACTURA ELECTRONICA Nro: {{$resolution->prefix}} - {{$request->number}}</title>
</head> --}}

<body>
    <table class="table" style="width: 100%">
        <tbody  >
            <tr>
                <td  style="width: 70px; font-size: 10px;"  >
                    C.C. o NIT:<br>
                    Cliente:<br>
                    @if(isset($request->ccliente))
                        {{$request->ccliente}}
                    @else
                        <br>
                    @endif
                    Dirección:<br>
                    Vendedor:<br>
                    E-mail:
                </td>
                    <td  style="width: 55%; font-size: 10px" >
                    {{$customer->company->identification_number}}-{{$request->customer["dv"] ?? NULL}}
                    <br/>
                    {{$customer->name}}<br/>
                    {{$request->sucursal}}
                    <br/>
                    {{$customer->company->address}}  {{$customer->company->municipality->name}} - {{$customer->company->country->name}}<br>
                    {{ $request->cvendedor}}<br/>
                    {{$customer->email}}
                </td>
                <td rowspan="2" style="width: 40px;  font-size: 9px" >
                    Pedido: <br>
                    Teléfono:<br>
                    Régimen:<br>
                    Forma Pago:<br>
                    Plazo Pago:<br>
                    Vencimiento:
                </td>
                <td   rowspan="2" style="width: 150px; font-size: 9px ">
                    {{$request->cpedido }}<br>
                    {{$customer->company->phone}}<br>
                    {{$customer->company->type_regime->name}}<br>
                    {{$paymentForm->name}}-{{$paymentForm->nameMethod}}<br>
                    {{$paymentForm->duration_measure}} Dias<br>
                    {{$paymentForm->payment_due_date}}
                </td>
                <td rowspan="2" style="width: 100px; border: none; background: none;  " >
                    <img id="imgQR" src="{{$imageQr}}">
=======
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    {{-- <title>NOTA DEBITO ELECTRONICA Nro: {{$resolution->prefix}} - {{$request->number}}</title> --}}
</head>
<body margin-top:50px>
    @if(isset($request->head_note))
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered table-condensed table-striped table-responsive">
                <thead>
                    <tr>
                        <th class="text-center"><p><strong>{{$request->head_note}}<br/>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    @endif
    <table style="width: 100%;">
        <tbody>
            <tr>
                <td>
                    <table style="width: 100%; font-size: 12px;">
                        <tbody>
                            <tr>
                                <td>CC o NIT:</td>
                                <td>{{$customer->company->identification_number}}-{{$request->customer['dv'] ?? NULL}} </td>
                            </tr>
                            <tr>
                                <td>Cliente:</td>
                                <td>{{$customer->name}}</td>
                            </tr>
                            <tr>
                                <td>Regimen:</td>
                                <td>{{$customer->company->type_regime->name}}</td>
                            </tr>
                            <tr>
                                <td>Obligación:</td>
                                <td>{{$customer->company->type_liability->name}}</td>
                            </tr>
                            <tr>
                                <td>Dirección:</td>
                                <td>{{$customer->company->address}}</td>
                            </tr>
                            <tr>
                                <td>Ciudad:</td>
                                @if($customer->company->country->id == 46)
                                    <td>{{$customer->company->municipality->name}} - {{$customer->company->country->name}} </td>
                                @else
                                    <td>{{$customer->company->municipality_name}} - {{$customer->company->state_name}} - {{$customer->company->country->name}} </td>
                                @endif
                            </tr>
                            <tr>
                                <td>Telefono:</td>
                                <td>{{$customer->company->phone}}</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>{{$customer->email}}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td class="vertical-align-top" style="width: 40%; padding-left: 1rem">
                    <table>
                        <tr>
                            <td>Forma de Pago:</td>
                            <td>{{$paymentForm->name}}</td>
                        </tr>
                        <tr>
                            <td>Medio de Pago:</td>
                            <td>{{$paymentForm->nameMethod}}</td>
                        </tr>
                        <tr>
                            <td>Plazo Para Pagar:</td>
                            <td>{{$paymentForm->duration_measure}} Dias</td>
                        </tr>
                        <tr>
                            <td>Fecha Vencimiento:</td>
                            <td>{{$paymentForm->payment_due_date}}</td>
                        </tr>
                        @if(isset($request['order_reference']['id_order']))
                        <tr>
                            <td>Numero Pedido:</td>
                            <td>{{$request['order_reference']['id_order']}}</td>
                        </tr>
                        @endif
                        @if(isset($request['order_reference']['issue_date_order']))
                        <tr>
                            <td>Fecha Pedido:</td>
                            <td>{{$request['order_reference']['issue_date_order']}}</td>
                        </tr>
                        @endif
                        @if(isset($healthfields))
                        <tr>
                            <td>Inicio Periodo Facturacion:</td>
                            <td>{{$healthfields->invoice_period_start_date}}</td>
                        </tr>
                        <tr>
                            <td>Fin Periodo Facturacion:</td>
                            <td>{{$healthfields->invoice_period_end_date}}</td>
                        </tr>
                        @endif
                        @if(isset($request['number_account']))
                        <tr>
                            <td>Número de cuenta:</td>
                            <td>{{ $request['number_account'] }}</td>
                        </tr>
                        @endif
                    </table>
                </td>

                <td class="vertical-align-top" style="width: 40%; padding-left: 1rem">
                    <table>
                        @if(isset($request['order_reference']['id_order']))
                        <tr>
                            <td>Numero Pedido:</td>
                            <td>{{$request['order_reference']['id_order']}}</td>
                        </tr>
                        @endif
                        @if(isset($request['order_reference']['issue_date_order']))
                        <tr>
                            <td>Fecha Pedido:</td>
                            <td>{{$request['order_reference']['issue_date_order']}}</td>
                        </tr>
                        @endif
                        @if(isset($healthfields))
                        <tr>
                            <td>Inicio Periodo Facturacion:</td>
                            <td>{{$healthfields->invoice_period_start_date}}</td>
                        </tr>
                        <tr>
                            <td>Fin Periodo Facturacion:</td>
                            <td>{{$healthfields->invoice_period_end_date}}</td>
                        </tr>
                        @endif
                    </table>
                </td>

                <td class="vertical-align-top" style="width: 30%; text-align: right">
                    <img style="width: 150px;" src="{{$imageQr}}">
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <table class="table" style="width: 100%">
        <tbody>
            <tr>
                <td style="text-align: center; padding: 15px 0;">
                    <?php $billing_reference = json_decode(json_encode($request->billing_reference)) ?>
                    <p style="font-size: 10px; font-weight: bold;">Referencia: {{$billing_reference->number}} - Fecha: {{$billing_reference->issue_date}}</p>
                    <p style="font-size: 10px; font-weight: bold;">CUFE: {{$billing_reference->uuid}}</p>
>>>>>>> 5f1f513048b194385f5c8367218425b7d63396ac
                </td>
            </tr>
        </tbody>
    </table>
<<<<<<< HEAD
=======
    <br>
    @isset($healthfields)
        <table class="table" style="width: 100%;">
            <thead>
                <tr>
                    <th class="text-center" style="width: 100%;">INFORMACION REFERENCIAL SECTOR SALUD</th>
                </tr>
            </thead>
        </table>
        <table class="table" style="width: 100%;">
            <thead>
                <tr>
                    <th class="text-center" style="width: 12%;">Cod Prestador</th>
                    <th class="text-center" style="width: 25%;">Datos Usuario</th>
                    <th class="text-center" style="width: 25%;">Info. Contrat./Cobertura</th>
                    <th class="text-center" style="width: 20%;">Nros. Autoriz./MIPRES</th>
                    <th class="text-center" style="width: 18%;">Info. de Pagos</th>
                </tr>
            </thead>
            <tbody>
                @foreach($healthfields->users_info as $item)
                    <tr>
                        <td>
                            <p style="font-size: 8px">{{$item->provider_code}}</p>
                        </td>
                        <td>
                            <p style="font-size: 8px">Nro ID: {{$item->identification_number}}</p>
                            <p style="font-size: 8px">Nombre: {{$item->first_name}} {{$item->surname}}</p>
                            <p style="font-size: 8px">Tipo Documento: {{$item->health_type_document_identification()->name}}</p>
                            <p style="font-size: 8px">Tipo Usuario: {{$item->health_type_user()->name}}</p>
                        </td>
                        <td>
                            <p style="font-size: 8px">Modalidad Contratacion: {{$item->health_contracting_payment_method()->name}}</p>
                            <p style="font-size: 8px">Nro. Contrato: {{$item->contract_number}}</p>
                            <p style="font-size: 8px">Cobertura: {{$item->health_coverage()->name}}</p>
                        </td>
                        <td>
                            <p style="font-size: 8px">Nros Autorizacion: {{$item->autorization_numbers}}</p>
                            <p style="font-size: 8px">Nro MIPRES: {{$item->mipres}}</p>
                            <p style="font-size: 8px">Entrega MIPRES: {{$item->mipres_delivery}}</p>
                            <p style="font-size: 8px">Nro Poliza: {{$item->policy_number}}</p>
                        </td>
                        <td>
                            <p style="font-size: 8px">Copago: {{number_format($item->co_payment, 2)}}</p>
                            <p style="font-size: 8px">Cuota Moderardora: {{number_format($item->moderating_fee, 2)}}</p>
                            <p style="font-size: 8px">Cuota Recuperacion: {{number_format($item->recovery_fee, 2)}}</p>
                            <p style="font-size: 8px">Pagos Compartidos: {{number_format($item->shared_payment, 2)}}</p>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <br>
    @endisset
>>>>>>> 5f1f513048b194385f5c8367218425b7d63396ac
    <table class="table" style="width: 100%;">
        <thead  >
            <tr>
                <th class="text-center">#</th>
                <th class="text-center" >Código</th>
                <th class="text-center">Descripcion</th>
                <th class="text-center">Cantidad</th>
                <th class="text-center">UM</th>
                <th class="text-center">Val. Unit</th>
                <th class="text-center">IVA/IC</th>
                <th class="text-center">V.Desc.</th>
                <th class="text-center">Val. Item</th>
            </tr>
        </thead>
        <tbody>
            <?php $ItemNro = 0; ?>
            @foreach($request['debit_note_lines'] as $item)
                <?php $ItemNro = $ItemNro + 1; ?>
                <tr>
                    @inject('um', 'App\UnitMeasure')
<<<<<<< HEAD
                    @if($item['description'] == 'Administración' or $item['description'] == 'Imprevisto' or $item['description'] == 'Utilidad')
                        <td>{{$ItemNro}}</td>
                        <td class="text-right">
                            {{$item['code']}}
                        </td>
                        <td>{{$item['description']}}</td>
                        <td class="text-right">{{number_format($item['price_amount'], 2)}}</td>
=======
                    <td>{{$ItemNro}}</td>
                    <td>{{$item['code']}}</td>
                    <td>
                        @if(isset($item['notes']))
                            {{$item['description']}}
                            <p style="font-style: italic; font-size: 7px"><strong>Nota: {{$item['notes']}}</strong></p>
                        @else
                            {{$item['description']}}
                        @endif
                    </td>
                    <td class="text-right">{{number_format($item['invoiced_quantity'], 2)}}</td>
                    <td class="text-right">{{$um->findOrFail($item['unit_measure_id'])['name']}}</td>
                    @if(isset($item['tax_totals'][0]['tax_amount']))
                        <td class="text-right">{{number_format($item['price_amount']  - ($item['tax_totals'][0]['tax_amount'] / $item['invoiced_quantity']), 2)}}</td>
                    @else
                        <td class="text-right">{{number_format($item['price_amount'], 2)}}</td>
                    @endif
                    @if(isset($item['tax_totals'][0]['tax_amount']))
>>>>>>> 5f1f513048b194385f5c8367218425b7d63396ac
                        <td class="text-right">{{number_format($item['tax_totals'][0]['tax_amount'], 2)}}</td>
                        @if(isset($item['allowance_charges']))
                            <td class="text-right">{{number_format($item['allowance_charges'][0]['amount'], 2)}}</td>
                        @else
                            <td class="text-right">{{number_format("0", 2)}}</td>
                        @endif
                        <td class="text-right">{{number_format($item['invoiced_quantity'] * $item['price_amount'], 2)}}</td>
                    @else
<<<<<<< HEAD
                        <td>{{$ItemNro}}</td>
                        <td>{{$item['code']}}</td>
                        <td>{{$item['description']}}</td>
                        <td class="text-right">{{number_format($item['invoiced_quantity'], 2)}}</td>
                        <td class="text-right">{{$um->findOrFail($item['unit_measure_id'])['name']}}</td>
                        <td class="text-right">{{number_format($item['price_amount'], 2)}}</td>
                        @if(isset($item['tax_totals']))
                            @if(isset($item['tax_totals'][0]['tax_amount']))
                                <td class="text-right">{{number_format($item['tax_totals'][0]['tax_amount'], 2)}}</td>
                            @else
                                <td class="text-right">{{number_format(0, 2)}}</td>
                            @endif
                        @else
                            <td class="text-right">E</td>
                        @endif
                        @if(isset($item['allowance_charges']))
                            <td class="text-right">{{number_format($item['allowance_charges'][0]['amount'], 2)}}</td>
                        @else
                            <td class="text-right">{{number_format("0", 2)}}</td>
                        @endif
=======
                        <td class="text-right">{{number_format(0, 2)}}</td>
                    @endif
                    @if(isset($item['allowance_charges']))
                        <td class="text-right">{{number_format($item['allowance_charges'][0]['amount'], 2)}}</td>
                        <td class="text-right">{{number_format($item['invoiced_quantity'] * $item['price_amount'] - $item['allowance_charges'][0]['amount'], 2)}}</td>
                    @else
                        <td class="text-right">{{number_format("0", 2)}}</td>
>>>>>>> 5f1f513048b194385f5c8367218425b7d63396ac
                        <td class="text-right">{{number_format($item['invoiced_quantity'] * $item['price_amount'], 2)}}</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
<<<<<<< HEAD
    <table class="table" id="tablaTotales">
=======
    <br>

    <table class="table" style="width: 100%;">
        <thead>
            <tr>
                <th class="text-center">Impuestos</th>
                <th class="text-center">Retenciones</th>
                <th class="text-center">Totales</th>
            </tr>
        </thead>
>>>>>>> 5f1f513048b194385f5c8367218425b7d63396ac
        <tbody>
            <tr>
                <td style="width: 27%;">
                    <table class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center">Nro Lineas: {{$ItemNro}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="border: none">
                                    <br><br><br><br><br><br>
                                    Firma:_______________  Identificacion:__________
                                </td>
                           </tr>
                        </tbody>
                    </table>
                </td>
                <td style="width: 50%;">
                    <table class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center">Impues.</th>
                                <th class="text-center">Base</th>
                                <th class="text-center">Valor</th>
                                <th class="text-center">Retenc.</th>
                                <th class="text-center">Base</th>
                                <th class="text-center">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($request->tax_totals))
                                <?php $TotalImpuestos = 0; ?>
                                @foreach($request->tax_totals as $item)
                                    <tr>
                                        <?php $TotalImpuestos = $TotalImpuestos + $item['tax_amount'] ?>
                                        @inject('tax', 'App\Tax')
                                        <td>{{$tax->findOrFail($item['tax_id'])['name']}}-<small>{{number_format($item['percent'], 2)}}%</small> </td>
                                        <td class="text-right" >{{number_format($item['taxable_amount'], 2)}}</td>
                                        <td class="text-right" >{{number_format($item['tax_amount'], 2)}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <?php $TotalImpuestos = 0; ?>
                            @endif
                            @if(isset($withHoldingTaxTotal))
                                <?php $TotalRetenciones = 0; ?>
                                @foreach($withHoldingTaxTotal as $item)
                                    <tr>
                                        <?php $TotalRetenciones = $TotalRetenciones + $item['tax_amount'] ?>
                                        @inject('tax', 'App\Tax')
                                        <td>{{$tax->findOrFail($item['tax_id'])['name']}}-<small>{{number_format($item['percent'], 2)}}%</small> </td>
                                        <td  class="text-right" >{{number_format($item['taxable_amount'], 2)}}</td>
                                        <td class="text-right" >{{number_format($item['tax_amount'], 2)}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            <tr>
                                @inject('Varios', 'App\Custom\NumberSpellOut')
                                @if(isset($request->tarifaica))
                                    <td colspan="6" class="totalf">SON: {{$Varios->convertir($request->legal_monetary_totals['payable_amount'] + $request->legal_monetary_totals['allowance_total_amount'])}} M/CTE*********.</td>
                                @else
                                    <td colspan="6" class="totalf">SON: {{$Varios->convertir($request->legal_monetary_totals['payable_amount'])}} M/CTE*********.</td>
                                @endif
                            </tr>
                            <tr>
                                @if(isset($notes))
                                    <td colspan="6" class="totalf">{{$notes}} </td>
                                @endif
                            </tr>
                    </tbody>
                    </table>
                </td>
                <td style="width: 17%;">
                    <table class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center">Totales</th>
                                <th class="text-center">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($request->legal_monetary_totals['line_extension_amount'] > 0)
                                <tr>
                                    <td>Base:</td>
                                    <td class="text-right" >{{number_format($request->legal_monetary_totals['line_extension_amount'], 2)}}</td>
                                </tr>
                            @endif

                            @if($TotalImpuestos > 0)
                                <tr>
                                    <td>Impuestos:</td>
                                    <td class="text-right" >{{number_format($TotalImpuestos, 2)}}</td>
                                </tr>
                             @endif
                             @if($TotalRetenciones > 0)
                                <tr>
                                    <td>Retenciones:</td>
                                    <td class="text-right" >{{number_format($TotalRetenciones, 2)}}</td>
                                </tr>
                             @endif

                             @if($request->legal_monetary_totals['allowance_total_amount'] > 0)
                                <tr>
                                    <td>Descuentos:</td>
                                    <td class="text-right" >{{number_format($request->legal_monetary_totals['allowance_total_amount'], 2)}}</td>
                                </tr>
                            @endif
                            <tr>
<<<<<<< HEAD
                                <td class="totalf">Total Nota:</td>
                                @if(isset($request->tarifaica))
                                    <td class="totalf" style="text-align: right">{{number_format($request->legal_monetary_totals['payable_amount'] + $request->legal_monetary_totals['allowance_total_amount'], 2)}}</td>
                                @else
                                    <td class="totalf" style="text-align: right">{{number_format($request->legal_monetary_totals['payable_amount'], 2)}}</td>
=======
                                <td>Nro Lineas:</td>
                                <td>{{$ItemNro}}</td>
                            </tr>
                            <tr>
                                <td>Base:</td>
                                <td>{{number_format($request->requested_monetary_totals['line_extension_amount'], 2)}}</td>
                            </tr>
                            <tr>
                                <td>Impuestos:</td>
                                <td>{{number_format($TotalImpuestos, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Retenciones:</td>
                                <td>{{number_format($TotalRetenciones, 2)}}</td>
                            </tr>
                            <tr>
                                <td>Descuentos:</td>
                                @if(isset($request->requested_monetary_totals['allowance_total_amount']))
                                    <td>{{number_format($request->requested_monetary_totals['allowance_total_amount'], 2)}}</td>
                                @else
                                    <td>{{number_format(0, 2)}}</td>
                                @endif
                            </tr>
                            <tr>
                                <td>Total Nota:</td>
                                @if(isset($request->tarifaica))
                                    @if(isset($request->requested_monetary_totals['allowance_total_amount']))
                                        <td>{{number_format(round($request->requested_monetary_totals['payable_amount'] + $request->requested_monetary_totals['allowance_total_amount'], 2), 2)}}</td>
                                    @else
                                        <td>{{number_format(round($request->requested_monetary_totals['payable_amount'] + 0, 2), 2)}}</td>
                                    @endif
                                @else
                                    <td>{{number_format(round($request->requested_monetary_totals['payable_amount'], 2), 2)}}</td>
>>>>>>> 5f1f513048b194385f5c8367218425b7d63396ac
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
<<<<<<< HEAD
=======
    <br>

    <table style="border-collapse: collapse; width: 100%;">
        <tbody>
            <tr>
                <td>
                    @inject('Varios', 'App\Custom\NumberSpellOut')
                    <p style="font-size: 12px; font-weight: bold;">NOTAS:</p>
                    <p style="font-style: italic; font-size: 9px">{{$notes}}</p>
                    <br>
                    @if(isset($request->tarifaica))
                        @if(isset($request->requested_monetary_totals['allowance_total_amount']))
                            <p style="font-size: 12px; font-weight: bold;">SON: <span style="font-weight: normal;">{{$Varios->convertir(round($request->requested_monetary_totals['payable_amount'] + $request->requested_monetary_totals['allowance_total_amount'], 2), $request->idcurrency)}} M/CTE*********.</span></p>
                        @else
                            <p style="font-size: 12px; font-weight: bold;">SON: <span style="font-weight: normal;">{{$Varios->convertir(round($request->requested_monetary_totals['payable_amount'] + 0, 2), $request->idcurrency)}} M/CTE*********.</span></p>
                        @endif
                    @else
                        @if(isset($request->requested_monetary_totals['allowance_total_amount']))
                            <p style="font-size: 12px; font-weight: bold;">SON: <span style="font-weight: normal;">{{$Varios->convertir(round($request->requested_monetary_totals['payable_amount'] + $request->requested_monetary_totals['allowance_total_amount'], 2), $request->idcurrency)}} M/CTE*********.</span></p>
                        @else
                            <p style="font-size: 12px; font-weight: bold;">SON: <span style="font-weight: normal;">{{$Varios->convertir(round($request->requested_monetary_totals['payable_amount'] + 0, 2), $request->idcurrency)}} M/CTE*********.</span></p>
                        @endif
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    <br>

    <table>
        <tbody>
            <tr>
                <td>
                    @if(isset($request->disable_confirmation_text))
                        @if(!$request->disable_confirmation_text)
                            <p style="font-size: 12px; font-style: italic;">CUALQUIER INQUIETUD SOBRE ESTE DOCUMENTO AL TELEFONO {{$company->phone}} o al e-mail {{$user->email}}<br>
                            <br>
                            <p style="font-size: 10px; font-weight: bold;">FIRMA ACEPTACIÓN:</p><br>
                            <p style="font-size: 10px; font-weight: bold;">CC:</p><br>
                            <p style="font-size: 10px; font-weight: bold;">FECHA:</p>
                        @endif
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
>>>>>>> 5f1f513048b194385f5c8367218425b7d63396ac
</body>
</html>

